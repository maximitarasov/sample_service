FROM openjdk:11.0.8-jre
MAINTAINER MTARASOV
COPY ./target/*.jar /app/application.jar
CMD java -jar /app/application.jar
