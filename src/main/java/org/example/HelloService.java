package org.example;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Hello Guy service
 *
 */
public class HelloService
{
    public static void main( String[] args ) throws IOException {
        ServerSocket serverSocket = new ServerSocket(9090);
        while (true) {
            Socket socket = serverSocket.accept();
            new Thread(new Worker(socket)).start();
        }
    }

    static class Worker implements Runnable {

        private Socket socket;

        Worker(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try(PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(this.socket.getOutputStream()))) {
                printWriter.println("Hello Guy !");
                printWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
